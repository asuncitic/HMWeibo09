//
//  AppDelegate.swift
//  微博009
//
//  Created by Romeo on 15/8/29.
//  Copyright © 2015年 itheima. All rights reserved.
//

import UIKit
import AFNetworking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        // 打印用户账户信息
        printLog(UserAccountViewModel.sharedUserAccount.userAccount)
        
        // 设置网络
        setupNetwork()
        // 设置外观
        setupAppearance()
        
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.backgroundColor = UIColor.whiteColor()
        
        window?.rootViewController = MainViewController()
        
        window?.makeKeyAndVisible()
        
        return true
    }
    
    /// 设置网络指示器
    private func setupNetwork() {
        // 设置网络指示器，一旦设置，发起网络请求，会在状态栏显示菊花，指示器只负责 AFN 的网络请求，其他网络框架不负责
        AFNetworkActivityIndicatorManager.sharedManager().enabled = true
        
        // 设置缓存大小 NSURLCache －> GET 请求的数据会被缓存
        // 缓存的磁盘路径: /Library/Caches/(application bundle id)
        // MATTT，内存缓存是 4M，磁盘缓存是 20M
        // 提示：URLSession 只有 dataTask 会被缓存，downloadTask / uploadTask 都不会缓存
        let cache = NSURLCache(memoryCapacity: 4 * 1024 * 1024, diskCapacity: 20 * 1024 * 1024, diskPath: nil)
        NSURLCache.setSharedURLCache(cache)
    }
    
    /**
    设置全局外观
    
    修改导航栏外观 - 修改要尽量早，一经设置，全局有效
    */
    private func setupAppearance() {
        UINavigationBar.appearance().tintColor = UIColor.orangeColor()
        UITabBar.appearance().tintColor = UIColor.orangeColor()
    }
}

